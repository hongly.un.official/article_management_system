package com.hongly.articlemanagement.articlemanagementsystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication
@EnableWebSecurity
public class ArticlemanagementsystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArticlemanagementsystemApplication.class, args);
	}
}
