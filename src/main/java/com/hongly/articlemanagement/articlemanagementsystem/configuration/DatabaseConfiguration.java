package com.hongly.articlemanagement.articlemanagementsystem.configuration;

import org.springframework.boot.autoconfigure.jms.artemis.ArtemisProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;
import java.sql.DriverManager;

@Configuration
public class DatabaseConfiguration {

    @Bean
    @Profile("database")
    public DataSource dataSource(){
        DriverManagerDataSource dmd = new DriverManagerDataSource();
        dmd.setDriverClassName("org.postgresql.Driver");
        dmd.setUrl("jdbc:postgresql://127.0.0.1:5432/sp_db");
        dmd.setUsername("postgres");
        dmd.setPassword("hongly");
        return dmd;
    }

    @Bean
    @Profile("memory")
    public DataSource inMemory(){
       EmbeddedDatabaseBuilder builder=new EmbeddedDatabaseBuilder();
       builder.setType(EmbeddedDatabaseType.H2);
       builder.addScripts("sql/tables.sql","sql/data.sql");
       return builder.build();
    }
}
