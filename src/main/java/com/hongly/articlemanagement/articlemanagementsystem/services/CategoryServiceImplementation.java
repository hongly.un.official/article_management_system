package com.hongly.articlemanagement.articlemanagementsystem.services;

import com.hongly.articlemanagement.articlemanagementsystem.models.Category;
import com.hongly.articlemanagement.articlemanagementsystem.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImplementation implements CategoryService {
    CategoryRepository categoryRepository;

    @Autowired
    public void setCategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    @Override
    public Category findOne(int id) {
        return categoryRepository.findOne(id);
    }
    @Override
    public void insert(Category category) {
        categoryRepository.insert(category);
    }
    @Override
    public void update(Category category) {
        categoryRepository.update(category);
    }
    @Override
    public void delete(int id) {
        categoryRepository.delete(id);
    }
}
