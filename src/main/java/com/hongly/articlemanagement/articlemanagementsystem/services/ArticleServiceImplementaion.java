package com.hongly.articlemanagement.articlemanagementsystem.services;

import com.hongly.articlemanagement.articlemanagementsystem.models.Article;
import com.hongly.articlemanagement.articlemanagementsystem.models.ArticleSearch;
import com.hongly.articlemanagement.articlemanagementsystem.repositories.ArticleRepository;
import com.hongly.articlemanagement.articlemanagementsystem.utility.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleServiceImplementaion implements ArticleService{

    ArticleRepository articleRepository;


    @Autowired
    public void setArticleRepository(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }


    @Override
    public boolean insert(Article article) {
        return articleRepository.insert(article);
    }

    @Override
    public List<Article> findWithLimit(int page, int limit) {
        return articleRepository.findWithLimit(page,limit);
    }

    @Override
    public boolean delete(int id) {
        return articleRepository.delete(id);
    }

    @Override
    public boolean update(Article article) {
        return articleRepository.update(article);
    }

    @Override
    public List<Article> findAll() {
        return articleRepository.findAll();
    }


    @Override
    public Article findOne(int id) {
        return articleRepository.findOne(id);
    }

    @Override
    public int count() {
        return articleRepository.count();
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public List<Article> findWithSearch(ArticleSearch search, Paging paging) {
        paging.setTotalCount(articleRepository.countAllArticles(search));
        return articleRepository.findWithSearch(search,paging);
    }
}
