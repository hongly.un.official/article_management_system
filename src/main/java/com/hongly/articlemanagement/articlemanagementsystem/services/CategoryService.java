package com.hongly.articlemanagement.articlemanagementsystem.services;

import com.hongly.articlemanagement.articlemanagementsystem.models.Category;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;


public interface CategoryService {
    List<Category> findAll();
    Category findOne(int id);

    public void insert(Category category);
    public void update(Category category);
    public void delete(int id);
}
