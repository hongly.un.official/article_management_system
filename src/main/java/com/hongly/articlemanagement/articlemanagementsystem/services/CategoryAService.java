package com.hongly.articlemanagement.articlemanagementsystem.services;

import com.hongly.articlemanagement.articlemanagementsystem.models.CategoryA;
import com.hongly.articlemanagement.articlemanagementsystem.repositories.CategoryARepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryAService {

    @Autowired
    CategoryARepo categoryARepo;
    public List<CategoryA> findCategory(){

        System.out.println(categoryARepo.findCategory());
        return categoryARepo.findCategory();
    }
}
