package com.hongly.articlemanagement.articlemanagementsystem.services;

import com.hongly.articlemanagement.articlemanagementsystem.models.Article;
import com.hongly.articlemanagement.articlemanagementsystem.models.ArticleSearch;
import com.hongly.articlemanagement.articlemanagementsystem.utility.Paging;

import java.util.List;

public interface ArticleService {
    boolean insert(Article article);
    boolean delete(int id);
    boolean update(Article article);
    List<Article> findAll();
    List<Article> findWithLimit(int page,int limit);
    Article findOne(int id);
    List<Article> findWithSearch(ArticleSearch search, Paging paging);
    int count();

}
