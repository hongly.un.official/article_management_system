package com.hongly.articlemanagement.articlemanagementsystem.controllers;

import com.hongly.articlemanagement.articlemanagementsystem.models.Article;
import com.hongly.articlemanagement.articlemanagementsystem.models.ArticleSearch;
import com.hongly.articlemanagement.articlemanagementsystem.models.Category;
import com.hongly.articlemanagement.articlemanagementsystem.models.CategoryA;
import com.hongly.articlemanagement.articlemanagementsystem.services.ArticleService;
import com.hongly.articlemanagement.articlemanagementsystem.services.CategoryAService;
import com.hongly.articlemanagement.articlemanagementsystem.services.CategoryService;
import com.hongly.articlemanagement.articlemanagementsystem.utility.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Controller
public class ArticleController {

    ArticleService articleService;
    CategoryService categoryService;
    @Value("${file.server.path}")
    String serverPath;
    @Value("${file.client.path}")
    String clientPath;


    @Autowired
    public void setArticleService(ArticleService articleService) {
        this.articleService = articleService;
    }
    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping(value={"/article","/"})
    String article(Model model,ArticleSearch search,Paging paging){
//        System.out.println(search.getTitle());
        List<Article> articles = articleService.findWithSearch(search,paging);

        model.addAttribute("state", "article");
        model.addAttribute("articles", articles);
        model.addAttribute("search",search);
        model.addAttribute("paging",paging);
        return "article";
    }

    @GetMapping("/add")
    String add(Model model){
        model.addAttribute("isAdd",true);
        model.addAttribute("article",new Article());
        model.addAttribute("categories",categoryService.findAll());
        model.addAttribute("state", "add");
        return "add";
    }
    @PostMapping("/add")
    String saveArticle(Model model, @Valid @ModelAttribute Article article, BindingResult bindingResult, @RequestParam("file")MultipartFile file){

        if(bindingResult.hasErrors()){
            model.addAttribute("categories",categoryService.findAll());
            model.addAttribute("isAdd",true);
            return "/add";
        }
        if(!file.isEmpty()){
            try {
                Files.copy(file.getInputStream(), Paths.get(serverPath, file.getOriginalFilename()));
                article.setThumbnail(clientPath+file.getOriginalFilename());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            model.addAttribute("categories",categoryService.findAll());
            model.addAttribute("isAdd",true);
            return "/add";
        }

        Category category=categoryService.findOne(article.getCategory().getId());
        article.getCategory().setCategoryName(category.getCategoryName());
        articleService.insert(article);
        return "redirect:/article";
    }

    @GetMapping("/article/delete/{id}")
    public String delete(@PathVariable("id") int id){

        articleService.delete(id);

        System.out.println(articleService.findAll());
        return "redirect:/article";
    }

    @GetMapping("/article/update/{id}")
    public String update(@PathVariable("id") int id,Model model){
        model.addAttribute("isAdd",false);
        model.addAttribute("categories",categoryService.findAll());
        model.addAttribute("article",articleService.findOne(id));
        return "add";
    }

    @PostMapping("/update")
    public String saveUpdate(@ModelAttribute Article article,@RequestParam("file") MultipartFile file,Model model){
        if(!file.isEmpty()){
            try {
                Files.copy(file.getInputStream(), Paths.get(serverPath,file.getOriginalFilename()));
                article.setThumbnail(clientPath+file.getOriginalFilename());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else {
            model.addAttribute("isAdd",false);
            return "add";
        }
        article.setCategory(categoryService.findOne(article.getCategory().getId()));
        articleService.update(article);
        return "redirect:/article";
    }
    @GetMapping("/article/detail/{id}")
    public String detail(@PathVariable("id") int id,Model model){
        model.addAttribute("state", "article/detail/"+id);
        model.addAttribute("article",articleService.findOne(id));
        System.out.println(articleService.findOne(id));
        return "detail";
    }
    @GetMapping("/login")
    public String count(){
        return "login";
    }

    @Autowired
    CategoryAService categoryAService;
    @GetMapping("/cate")
    public String cate(Model model){

        List<CategoryA> lst =categoryAService.findCategory();
        System.out.println(lst);
        model.addAttribute("categories",lst);
        return "index";
    }
}
