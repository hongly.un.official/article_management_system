package com.hongly.articlemanagement.articlemanagementsystem.controllers;

import com.hongly.articlemanagement.articlemanagementsystem.models.Article;
import com.hongly.articlemanagement.articlemanagementsystem.models.ArticleSearch;
import com.hongly.articlemanagement.articlemanagementsystem.services.ArticleService;
import com.hongly.articlemanagement.articlemanagementsystem.utility.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/v1/api")
public class AdminRESTController {

    @Autowired
    ArticleService articleService;

    @GetMapping("/document")
    public List<Article> article( ArticleSearch search, Paging paging){
        return articleService.findWithSearch(search,paging);

    }
}
