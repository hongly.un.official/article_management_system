package com.hongly.articlemanagement.articlemanagementsystem.controllers;


import com.hongly.articlemanagement.articlemanagementsystem.models.Article;
import com.hongly.articlemanagement.articlemanagementsystem.services.ArticleService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/api")
public class ArticleRESTController {

    ArticleService articleService;

    public ArticleRESTController(ArticleService articleService) {
        this.articleService = articleService;
    }
    @GetMapping("/articles")
    public List<Article> article(){
        return articleService.findAll();
    }

    @GetMapping("/articles/{id}")
    public Article article(@PathVariable("id") int id){
        return articleService.findOne(id);
    }

    @DeleteMapping("/articles/{id}")
    public boolean  delete(@PathVariable("id") int id){
        return articleService.delete(id);
    }

    @PostMapping("/articles")
    public boolean  insert(@RequestBody() Article article){
        return articleService.insert(article);

    }


    @PutMapping("/articles")
    public boolean update(@RequestBody Article article){

        return articleService.update(article);
    }
}


