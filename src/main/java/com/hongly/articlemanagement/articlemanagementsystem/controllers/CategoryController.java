package com.hongly.articlemanagement.articlemanagementsystem.controllers;

import com.hongly.articlemanagement.articlemanagementsystem.models.Category;
import com.hongly.articlemanagement.articlemanagementsystem.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class CategoryController {

    CategoryService categoryService;

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/category")
    public String category(Model model){

        model.addAttribute("state", "category");
        model.addAttribute("isAdd",true);
        model.addAttribute("category",new Category());
        model.addAttribute("categories",categoryService.findAll());
        return "category";
    }
    @PostMapping("/category/add")
    public String saveCategory(@ModelAttribute Category category){
        categoryService.insert(category);
        return "redirect:/category";
    }
    @GetMapping("/category/update/{id}")
    public String update(@PathVariable int id,Model model){
        model.addAttribute("isAdd",false);
        model.addAttribute("category",categoryService.findOne(id));
        model.addAttribute("categories",categoryService.findAll());
        return "category";
    }
    @PostMapping("/category/update")
    public String saveUpdate(@ModelAttribute Category category){
        categoryService.update(category);
        return "redirect:/category";
    }
    @GetMapping("/category/delete/{id}")
    public String delete(@PathVariable int id){
        categoryService.delete(id);
        return "redirect:/category";
    }


}
