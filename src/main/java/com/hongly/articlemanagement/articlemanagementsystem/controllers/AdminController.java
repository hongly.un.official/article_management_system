package com.hongly.articlemanagement.articlemanagementsystem.controllers;



import com.hongly.articlemanagement.articlemanagementsystem.services.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AdminController {


    @Autowired
    ArticleService articleService;

    @GetMapping("/categories")
    public String categories(){
        return "categorie";
    }

    @GetMapping("/comments")
    public String comments(){
        return "comment";
    }

    @GetMapping("/dashboard")
    public String dashboard(){

        return "admin/dashboard";
    }


    @GetMapping("/document")
    public String documents(Model model){
        model.addAttribute("articles", articleService.findAll());
        return "admin/document";
    }


    @GetMapping("/feedbacks")
    public String feedbacks(){
        return "feedback";
    }

    @GetMapping("/reports")
    public String reports(){
        return "report";
    }

    @GetMapping("/users")
    public String users(){
        return "user";
    }

    @GetMapping("/test")
    public String test(){
        return "admin/layout/test";
    }

}
