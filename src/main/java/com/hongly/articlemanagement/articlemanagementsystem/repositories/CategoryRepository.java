package com.hongly.articlemanagement.articlemanagementsystem.repositories;

import com.hongly.articlemanagement.articlemanagementsystem.models.Category;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface CategoryRepository {



    @Select("SELECT id,category_name FROM tb_categories ORDER BY id ASC")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "categoryName", column = "category_name")
    })
    public List<Category> findAll();
    @Select("SELECT id,category_name FROM tb_categories WHERE id=#{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "categoryName", column = "category_name")
    })
    public Category findOne(int id);
    @Insert("INSERT INTO tb_categories(category_name) VALUES(#{categoryName})")
    public void insert(Category category);
    @Update("UPDATE tb_categories SET category_name=#{categoryName} WHERE id=#{id}")
    public void update(Category category);
    @Delete("DELETE FROM tb_categories WHERE id=#{id}")
    public void delete(int id);

}
