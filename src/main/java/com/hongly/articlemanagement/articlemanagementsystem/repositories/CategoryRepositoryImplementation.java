package com.hongly.articlemanagement.articlemanagementsystem.repositories;

import com.hongly.articlemanagement.articlemanagementsystem.models.Category;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

//@Repository
public class CategoryRepositoryImplementation implements CategoryRepository {

    List<Category> categories = new ArrayList<>();

    public CategoryRepositoryImplementation(){
       // for(int i=0;i<5;i++){
            categories.add(new Category(1,"Spring"));
            categories.add(new Category(2,"Web"));
            categories.add(new Category(3,"java"));
        //}

    }

    @Override
    public List<Category> findAll() {
        return categories;
    }
    @Override
    public Category findOne(int id) {
        for (int i=0;i<categories.size();i++) {
            if (categories.get(i).getId() == id) {
                return categories.get(i);
            }
        }
        return null;
    }

    @Override
    public void insert(Category category) {

    }

    @Override
    public void update(Category category) {

    }

    @Override
    public void delete(int id) {

    }
}
