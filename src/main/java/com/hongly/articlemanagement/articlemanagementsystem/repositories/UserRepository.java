package com.hongly.articlemanagement.articlemanagementsystem.repositories;

import com.hongly.articlemanagement.articlemanagementsystem.models.Role;
import com.hongly.articlemanagement.articlemanagementsystem.models.User;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository {

    @Select("SELECT * FROM tb_users WHERE email LIKE #{email}")
    @Results({
            @Result(property = "id",column = "user_id"),
            @Result(property = "roles",column = "user_id", many = @Many(select = "findRoleByUserID"))
    })
    public User findUserByEmail(String email);

//    @Select("SELECT r.role_id,r.role FROM tb_roles r INNER JOIN tb_user_role ur ON r.role_id = ur.role_id " +
//            "WHERE ur.user_id = #{id}")

    @Select("SELECT roles FROM tb_users WHERE user_id = #{id}")
    public List<Role> findRoleByUserID(@Param("id") int id);
}
