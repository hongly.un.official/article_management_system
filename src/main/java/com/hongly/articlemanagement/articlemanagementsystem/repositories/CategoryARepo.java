package com.hongly.articlemanagement.articlemanagementsystem.repositories;

import com.hongly.articlemanagement.articlemanagementsystem.models.CategoryA;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryARepo {

    @Select("SELECT cat_id,name,parent_id FROM akd_categories WHERE level=0")
    @Results({
            @Result(property = "id", column = "cat_id"),
            @Result(property = "parentId", column = "parent_id"),
            @Result(property = "subCategory", column = "cat_id",many = @Many(select = "findSubCategoryLevelOne")),
    })
    public List<CategoryA> findCategory();

    @Select("SELECT cat_id,name,parent_id FROM akd_categories WHERE parent_id=#{catID} AND level=1")
    @Results({
            @Result(property = "id", column = "cat_id"),
            @Result(property = "parentId", column = "parent_id"),
            @Result(property = "subCategory", column = "cat_id",many = @Many(select = "findSubCategoryLevelTwo")),
    })
    public List<CategoryA> findSubCategoryLevelOne(@Param("catID") String id);

    @Select("SELECT cat_id,name,parent_id FROM akd_categories WHERE parent_id=#{catID} AND level=2")
    @Results({
            @Result(property = "id", column = "cat_id"),
            @Result(property = "parentId", column = "parent_id"),
    })
    public List<CategoryA> findSubCategoryLevelTwo(@Param("catID") String id);
}
