package com.hongly.articlemanagement.articlemanagementsystem.repositories;

import com.hongly.articlemanagement.articlemanagementsystem.models.Article;
import com.hongly.articlemanagement.articlemanagementsystem.models.ArticleSearch;
import com.hongly.articlemanagement.articlemanagementsystem.provider.ArticleProvider;
import com.hongly.articlemanagement.articlemanagementsystem.utility.Paging;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ArticleRepository {

    @Insert("INSERT INTO tb_articles(title,description,author,thumbnail,created_date,category_id) " +
            "VALUES(#{title},#{description},#{author},#{thumbnail},#{createdDate},#{category.id})")
    boolean insert(Article article);
    @Select("SELECT a.id,a.title,a.description,a.author,a.thumbnail,a.created_date,a.category_id,c.category_name " +
            "FROM tb_articles a INNER JOIN tb_categories c ON a.category_id = c.id ORDER BY a.id ASC")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "title", column = "title"),
            @Result(property = "description", column = "description"),
            @Result(property = "author", column = "author"),
            @Result(property = "thumbnail", column = "thumbnail"),
            @Result(property = "createdDate", column = "created_date"),
            @Result(property = "category.id", column = "category_id"),
            @Result(property = "category.categoryName", column = "category_name")
    })
    List<Article> findAll();

    @Select("SELECT a.id,a.title,a.description,a.author,a.thumbnail,a.created_date,a.category_id,c.category_name " +
            "FROM tb_articles a INNER JOIN tb_categories c ON a.category_id = c.id WHERE a.id=#{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "title", column = "title"),
            @Result(property = "description", column = "description"),
            @Result(property = "author", column = "author"),
            @Result(property = "thumbnail", column = "thumbnail"),
            @Result(property = "createdDate", column = "created_date"),
            @Result(property = "category.id", column = "category_id"),
            @Result(property = "category.categoryName", column = "category_name")
    })
    Article findOne(int id);
    @Update("UPDATE tb_articles SET title=#{title},description=#{description},author=#{author},thumbnail=#{thumbnail},created_date=#{createdDate},category_id=#{category.id}" +
            "WHERE id=#{id}")
    boolean update(Article article);
    @Delete("DELETE FROM tb_articles WHERE id=#{id}")
    boolean delete(int id);
    @Select("SELECT COUNT(id) FROM tb_articles")
    public int count();

    @Select("SELECT a.id,a.title,a.description,a.author,a.thumbnail,a.created_date,a.category_id,c.category_name " +
            "FROM tb_articles a INNER JOIN tb_categories c ON a.category_id = c.id ORDER BY a.id ASC OFFSET #{page} LIMIT #{limit}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "title", column = "title"),
            @Result(property = "description", column = "description"),
            @Result(property = "author", column = "author"),
            @Result(property = "thumbnail", column = "thumbnail"),
            @Result(property = "createdDate", column = "created_date"),
            @Result(property = "category.id", column = "category_id"),
            @Result(property = "category.categoryName", column = "category_name")
    })
    List<Article> findWithLimit(@Param("page") int page,@Param("limit") int limit);

    @SelectProvider(method = "findWithSearch",type = ArticleProvider.class)
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "title", column = "title"),
            @Result(property = "description", column = "description"),
            @Result(property = "author", column = "author"),
            @Result(property = "thumbnail", column = "thumbnail"),
            @Result(property = "createdDate", column = "created_date"),
            @Result(property = "category.id", column = "category_id"),
            @Result(property = "category.categoryName", column = "category_name")
    })
    List<Article> findWithSearch(@Param("search")ArticleSearch search,@Param("paging") Paging paging);

    @SelectProvider(method="countAllArticles", type=ArticleProvider.class)
    public Integer countAllArticles(ArticleSearch search);
}

