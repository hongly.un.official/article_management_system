package com.hongly.articlemanagement.articlemanagementsystem.repositories;

import com.github.javafaker.Faker;
import com.hongly.articlemanagement.articlemanagementsystem.models.Article;
import com.hongly.articlemanagement.articlemanagementsystem.models.Category;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


////@Repository
//public class ArticleRepositoryImplementation implements ArticleRepository{
//
//    List<Article> articles= new ArrayList<>();
//    Faker dataFaker= new Faker();
//    public ArticleRepositoryImplementation() {
//        for(int i=0;i<10;i++){
//            articles.add(new Article(i,dataFaker.book().title(),dataFaker.book().genre(),new Date().toString(),dataFaker.book().author(),new Category(i,"Cateory"),dataFaker.internet().image()));
//        }
//    }
//
//    @Override
//    public boolean insert(Article article) {
//        articles.add(article);
//        return true;
//    }
//
//    @Override
//    public void delete(int id) {
//        for (int i=0;i<articles.size();i++){
//            if(articles.get(i).getId()==id){
//                articles.remove(articles.get(i));
//            }
//        }
//    }
//
//    @Override
//    public int count() {
//        return 0;
//    }
//
//    @Override
//    public boolean update(Article article) {
//        for (int i=0;i<articles.size();i++){
//            if(articles.get(i).getId()==article.getId()){
//                articles.get(i).setTitle(article.getTitle());
//                articles.get(i).setDescription(article.getDescription());
//                articles.get(i).setAuthor(article.getAuthor());
//                articles.get(i).setCategory(article.getCategory());
//                articles.get(i).setThumbnail(article.getThumbnail());
//            }
//        }
//        return true;
//    }
//
//    @Override
//    public List<Article> findAll() {
//        return articles;
//    }
//
//    @Override
//    public Article findOne(int id) {
//        for(int i=0;i<articles.size();i++) {
//            if(articles.get(i).getId() == id) {
//                return articles.get(i);
//            }
//        }
//        return null;
//    }
//}
