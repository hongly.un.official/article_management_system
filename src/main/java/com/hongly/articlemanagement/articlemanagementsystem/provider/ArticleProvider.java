package com.hongly.articlemanagement.articlemanagementsystem.provider;

import com.hongly.articlemanagement.articlemanagementsystem.models.ArticleSearch;
import com.hongly.articlemanagement.articlemanagementsystem.utility.Paging;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

public class ArticleProvider {
    public String findWithSearch(@Param("search") ArticleSearch search, @Param("paging") Paging paging){
        return new SQL(){{
            SELECT("a.id,a.title,a.description,a.author,a.thumbnail,a.created_date,a.category_id,c.category_name");
            FROM("tb_articles a");
            INNER_JOIN("tb_categories c ON a.category_id = c.id");
            if(search.getTitle()!=null)
                WHERE("a.title ILIKE '%'|| #{search.title} || '%'");
            ORDER_BY("a.id ASC LIMIT #{paging.limit} OFFSET #{paging.offset}");
        }}.toString();
    }

    public String countAllArticles(ArticleSearch filter) {
        return new SQL() {{
            SELECT("COUNT(id)");
            FROM("tb_articles");
            if (filter.getTitle() != null)
                WHERE("title ILIKE '%' || #{title} || '%'");

        }}.toString();
    }
}
