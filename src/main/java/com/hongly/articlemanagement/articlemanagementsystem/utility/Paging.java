package com.hongly.articlemanagement.articlemanagementsystem.utility;

public class Paging {
    private int page;
    private int limit;
    private int nextPage;
    private int previousPage;
    private int totalCount;
    private int totalPages;
    private int pagesToShow;
    private int startPage;
    private int endPage;

    private int offset;

    public Paging() {
        this(1,5,0,0,5);

    }

    public Paging(int page, int limit, int totalCount, int totalPages, int pagesToShow) {
        this.page = page;
        this.limit = limit;
        this.totalCount = totalCount;
        this.totalPages = totalPages;
        this.pagesToShow = pagesToShow;
    }

    public void setPage(int page) {
        this.page = (page<=1)?1:page;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public void setNextPage(int nextPage) {
        this.nextPage = nextPage;
    }

    public void setPreviousPage(int previousPage) {
        this.previousPage = previousPage;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
        this.setStartPageEndPage(getTotalPages());
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public void setPagesToShow(int pagesToShow) {
        this.pagesToShow = pagesToShow;
    }

    public void setStartPage(int startPage) {
        this.startPage = startPage;
    }

    public void setEndPage(int endPage) {
        this.endPage = endPage;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getPage() {
        return page;
    }

    public int getLimit() {
        return limit;
    }

    public int getNextPage() {
        return (int) (page >= getTotalPages() ? getTotalPages() : page + 1);
    }

    public int getPreviousPage() {
        return (page <= 1) ? 1 : page - 1;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public int getTotalPages() {
        return (int)Math.ceil((double)this.totalCount/limit);
    }

    public int getPagesToShow() {
        return pagesToShow;
    }

    public int getStartPage() {
        return startPage;
    }

    public int getEndPage() {
        return endPage;
    }

    public int getOffset() {
        return (this.page-1)*this.limit;
    }

    // TODO:
    private void setStartPageEndPage(int totalPages) {
        int halfPagesToShow = pagesToShow / 2;

        if (totalPages <= pagesToShow) {
            startPage = 1;
            endPage = totalPages;

        } else if (page - halfPagesToShow <= 0) {
            startPage = 1;
            endPage = pagesToShow;

        } else if (page + halfPagesToShow == totalPages) {
            startPage = page - halfPagesToShow;
            endPage = totalPages;

        } else if (page + halfPagesToShow > totalPages) {
            startPage = totalPages - pagesToShow + 1;
            endPage = totalPages;

        } else {
            startPage = page - halfPagesToShow;
            endPage = page + halfPagesToShow;
        }
    }
}
