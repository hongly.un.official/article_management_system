package com.hongly.articlemanagement.articlemanagementsystem.models;

import java.util.List;

public class CategoryA {
    private String id;
    private String name;
    private String parentId;
    private List<CategoryA> subCategory;

    public CategoryA(){}

    public CategoryA(String id, String name, String parentId, List<CategoryA> subCategory) {
        this.id = id;
        this.name = name;
        this.parentId = parentId;
        this.subCategory = subCategory;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public List<CategoryA> getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(List<CategoryA> subCategory) {
        this.subCategory = subCategory;
    }

    @Override
    public String toString() {
        return "CategoryA{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", parentId='" + parentId + '\'' +
                ", subCategory=" + subCategory +
                '}';
    }
}
