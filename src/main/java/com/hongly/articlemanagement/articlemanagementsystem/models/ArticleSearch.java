package com.hongly.articlemanagement.articlemanagementsystem.models;

public class ArticleSearch {
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
