package com.hongly.articlemanagement.articlemanagementsystem.models;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Article {

    private int id;
    @NotEmpty(message="Can't be empty")
    @Size(min=2,message = "Length must greater than two character")
    private String title;
    private String description;
    private String createdDate;
    @NotEmpty
    private String author;
    private Category category;
    private String thumbnail;
    public Article(){
    }
    public Article(int id, String title, String description, String createdDate, String author, Category category, String thumbnail) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.createdDate = createdDate;
        this.author = author;
        this.category = category;
        this.thumbnail = thumbnail;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }


    @Override
    public String toString() {
        return "Article{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", createdDate='" + createdDate + '\'' +
                ", author='" + author + '\'' +
                ", category=" + category +
                ", thumbnail='" + thumbnail + '\'' +
                '}';
    }
}
