var page=1;

function getNewDocumets(direction) {

    if(direction=='next'){
        page++;
    }else {
        page--;
    }
    console.log(page);
    var newUploadedDocument= $("#new-document");
        newUploadedDocument.empty();
    $.ajax({
        type: "GET",
        url: "/api/v1/documents/latest?page=" + page,
        success: function (respone) {
            console.log(respone)
            var data = respone.data;
            for(var index in data){

               var text= "<div class='col-sm-4 col-md-3 box-product-outer'>" +
                            "<div class='box-product'>" +
                                "<div class='img-wrapper'>" +
                                    "<a href='detail.html'>" +
                                        "<img alt='Document' src="+data[index].thumbnail+">" +
                                            "</a>" +
                                                "<div class='option'>" +
                                                    "<a href="+data[index].exportLink+" data-toggle='tooltip' data-placement='bottom' <i class='ace-icon fa fa-cloud-download'></i></a>" +
                                                    "<a href='#' data-toggle='tooltip' data-placement='bottom'  ><i class='ace-icon fa fa-eye'></i></a>" +
                                                    "<a href='#' data-toggle='tooltip' data-placement='bottom' ><i class='ace-icon fa fa-heart'></i></a>"+
                                                "</div>" +
                                "</div>" +
                                "<h6><a href='detail.html'>"+data[index].title+"</a></h6>" +
                                "<div class='price'>" +
                                    "<span><i class='ace-icon fa fa-user'></i>"+data[index].user.name+"</span>" +
                                    "<div class='rating'>" +
                                        "<form method='get' action='file.doc'>" +

                                        "</form>" +
                                        "<span class='view'>"+data[index].view+" <i class='fa fa-eye'></i></span>" +
                                    "</div>" +
                                "</div>" +
                            "</div>" +
                        "</div>";

                newUploadedDocument.append(text);

            }


        },
        error: function (err) {
            console.log(err)
        }
    });
}

var mostViewedPage=1;
function getMostViewedDocumets(direction) {

    if(direction=='next'){
        mostViewedPage++;
    }else {
        mostViewedPage--;
    }
    console.log(mostViewedPage);

    var mostViewed= $("#most-viewed");
        mostViewed.empty();
    $.ajax({
        type: "GET",
        url: "/api/v1/documents/most-view?page=" + mostViewedPage,
        success: function (respone) {
            console.log(respone)
            var data = respone.data;
            for(var index in data){

                var text= "<div class='col-sm-4 col-md-3 box-product-outer'>" +
                    "<div class='box-product'>" +
                    "<div class='img-wrapper'>" +
                    "<a href='detail.html'>" +
                    "<img alt='Document' src="+data[index].thumbnail+">" +
                    "</a>" +
                    "<div class='option'>" +
                    "<a href="+data[index].exportLink+" data-toggle='tooltip' data-placement='bottom' <i class='ace-icon fa fa-cloud-download'></i></a>" +
                    "<a href='#' data-toggle='tooltip' data-placement='bottom'  ><i class='ace-icon fa fa-eye'></i></a>" +
                    "<a href='#' data-toggle='tooltip' data-placement='bottom' ><i class='ace-icon fa fa-heart'></i></a>"+
                    "</div>" +
                    "</div>" +
                    "<h6><a href='detail.html'>"+data[index].title+"</a></h6>" +
                    "<div class='price'>" +
                    "<span><i class='ace-icon fa fa-user'></i>"+data[index].user.name+"</span>" +
                    "<div class='rating'>" +
                    "<form method='get' action='file.doc'>" +

                    "</form>" +
                    "<span class='view'>"+data[index].view+" <i class='fa fa-eye'></i></span>" +
                    "</div>" +
                    "</div>" +
                    "</div>" +
                    "</div>";

                mostViewed.append(text);

            }
        },
        error: function (err) {
            console.log(err)
        }
    });
}


$(document).ready(function (){
    getNewDocumets(1);
    getMostViewedDocumets(1);
});

