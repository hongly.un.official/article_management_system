
CREATE TABLE tb_articles(
  id INT PRIMARY KEY auto_increment,
  title VARCHAR NOT NULL,
  description VARCHAR,
  author varchar NOT NULL,
  thumbnail VARCHAR,
  created_date VARCHAR NOT NULL,
  category_id INT NOT NULL
);

CREATE TABLE tb_categories(
  id INT PRIMARY KEY auto_increment,
  category_name VARCHAR NOT NULL
);




